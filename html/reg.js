﻿// 1.显示用户选择的国家编码，如果没有选择，则默认显示+86
if(localStorage.countryNo){
  s1.innerHTML = "+"+localStorage.countryNo;
}else{
  s1.innerHTML = "+86";
}


// 2.跳转至国家编码选择页面
function toSelectCountry(){
  location = "1.html";
}

// 3.获取验证码
var timer;
var rightYZM;
function getYZM(){
  $.post("http://47.104.241.232/yzm.php",{phone:'18323456543'},function(data){
    console.log(data);
    rightYZM = data;
  });

  yzm_btn.value = "重新发送60";

  timer = setInterval(f,1000);
}

var i = 60;

function f(){
  i--;
  if(i==0){
    i = 60;
    clearInterval(timer);
    yzm_btn.value = "获取验证码";
    return;
  }
  yzm_btn.value = "重新发送"+i;
}

//4.检测手机号
function check_phone(){
  var pv = phone.value;
  var reg = /^1([38]\d|5[0-35-9]|7[3678])\d{8}$/;

  if(reg.test(pv)){
     phone_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/tick.png' />"
  }else{
     phone_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/error.png' /> 您输入的手机号不正确！"
  }
}

//5.检测验证码
function check_yzm(){
  var yv = yzm.value;
  
  if(rightYZM==yv){
     yzm_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/tick.png' />"
  }else{
     yzm_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/error.png' /> 您输入的验证码有误！"
  }
}

//6. 验证密码规范性
function check_mima(){
  var mv = mima.value;
  
  if(mv.length>=6){
     mima_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/tick.png' />"
  }else{
     mima_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/error.png' /> 密码长度不能少于6位！"
  }
}

//7. 验证确认密码
function check_mima2(){
  var mv2 = mima2.value;
  
  if(mv2==mima.value){
     mima2_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/tick.png' />"
  }else{
     mima2_info.innerHTML = "<img src='https://4.url.cn/zc/v3/img/error.png' /> 两次密码输入不一致！"
  }
}

//8.注册
function reg(){
  $.post("http://47.104.241.232/add2.php",{
    username:'',
    pwd:mima.value,
    phone:phone.value
  },function(data){
    if(data=="ok"){
      alert("注册成功！");
    }else{
      alert("注册失败！");
    }
  });
}













